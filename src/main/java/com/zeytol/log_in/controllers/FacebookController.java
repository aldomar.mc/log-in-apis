package com.zeytol.log_in.controllers;

import com.zeytol.log_in.utils.Constant;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/facebook")
public class FacebookController {
    public static final String URL_APIREST_FACEBOOK = Constant.URL_APIREST_FACEBOOK;
    private final Logger log = LoggerFactory.getLogger(getClass());

    /*
     * Obtenemos la url de autorización
     * */
    @GetMapping("/generateFacebookAuthorizeUrl")
    public String generateFacebookAuthorizeUrl() {
        final String url = URL_APIREST_FACEBOOK + "/generateFacebookAuthorizeUrl";
        RestTemplate restTemplate = new RestTemplate();
        String urlAuthorize = restTemplate.getForObject(url, String.class);
        log.info("[Authorize Url]: " + urlAuthorize);
        return urlAuthorize;
    }

    /*
     * Obtenemos los datos del usuario
     * */
    @GetMapping("/getUserData")
    public JSONObject getUserData() {
        final String url = URL_APIREST_FACEBOOK + "/getUserData";
        RestTemplate restTemplate = new RestTemplate();
        String data = restTemplate.getForObject(url, String.class);
        //convertimos la cadena en formato json
        JSONObject json = new JSONObject(data);
        log.info("[User data]: " + json);
        return json;
    }
}
