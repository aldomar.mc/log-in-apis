package com.zeytol.log_in.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @Autowired
    private FacebookController facebookController;

    @GetMapping
    public String login(Model model) {
        model.addAttribute("loginFacebook", facebookController.generateFacebookAuthorizeUrl());
        return "log_in";
    }

    @GetMapping("/home")
    public String home(Model model) {
        model.addAttribute("data_user", facebookController.getUserData());
        return "home";
    }
}
